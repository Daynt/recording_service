//
// Created by user on 28/05/15.
//

#include <iostream>
#include "config.h"


config::config(string filename) {
    path = filename;
}

bool config::load(){
    if (FILE *fp = fopen(path.c_str(), "rb")) {
        char readBuffer[65536];
        FileReadStream is(fp, readBuffer, sizeof(readBuffer));
        file.ParseStream(is);
        fclose(fp);
    }
    else
        return false;

    return true;

}


bool config::check(){

    if (file == NULL){
        cerr << "[-] Config is not loaded" << endl;
        return false;
    }
    bool found = false;
    for (int i = 0; i < required.size() ; ++i) {
        if(!file.HasMember(required[i].c_str())){
            if(!found)
                found = true;
            cerr << "[-] Config value " + required[i] + " is missing" << endl;
        }
    }
    return !found;
};
