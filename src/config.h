//
// Created by user on 28/05/15.
//

#ifndef RECORDING_CONFIG_H
#define RECORDING_CONFIG_H

#include <cstdio>
#include <string>
#include <vector>
#include <rapidjson/filereadstream.h>
#include <rapidjson/document.h>


using namespace std;
using namespace rapidjson;


class config {
    string path;
    vector <string> required = {"db_host", "db_port", "db_username", "db_password", "db_name", "recordsFolder"};
    public:
        config(string filename);
        bool load();
        Document file = NULL;
        bool check();
};


#endif //RECORDING_CONFIG_H
