//
// Created by user on 30/05/15.
//

#include "database.h"


database::database(string host, unsigned short prt, string user, string pass, string dbName){
    hostname = host;
    port = prt;
    username = user;
    password = pass;
    databaseName = dbName;

};

database::~database(){
    delete(db);
    delete(rgx);
}

bool database::connect(){
    db = new mysqlWrapper(hostname, port, username, password, databaseName);
    return db->connect();
}

void database::close(){
    db->close();
}

vector<pair<int,string>> database::getStreams(){
    if(!db->query("Select id,url from streams")){
        exit(0);
    }
    vector<vector <string>> rows = db->fetchAll();
    vector<pair<int,string>> urls;
    for (int i = 0; i < rows.size(); ++i) {
        if(regex_search(rows[i][1], (*rgx)))
            urls.push_back(make_pair(stoi(rows[i][0]),rows[i][1]));
    }

    return urls;
}

bool database::createRecord(int streamID, string path){
    if(!db->query("INSERT INTO `recording`.`records` (`streamid`, `path`, `date`) VALUES ('" + to_string(streamID) + "', '"+ db->escape(path) + "', CURRENT_TIMESTAMP)")){
        exit(0);
    }
    return true;
}

bool database::switchOnlineStatus(int streamID){
    if(!db->query("UPDATE streams SET online = NOT online WHERE id = " + to_string(streamID))){
        exit(0);
    }
    return true;

}

bool database::setAllOffline(){
    if(!db->query("UPDATE `streams` SET `online` = 0")){
        exit(0);
    }
    return true;

}