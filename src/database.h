//
// Created by user on 30/05/15.
//

#ifndef RECORDING_DATABASE_H
#define RECORDING_DATABASE_H

#include <string>
#include <vector>
#include <regex>
#include "mysqlWrapper.h"

using namespace std;

class database {
    string hostname;
    unsigned short port;
    string databaseName;
    string username;
    string password;
    regex* rgx = new regex("http?[s]:\\/\\/.*?\\/[^\\/]+");
    mysqlWrapper* db;
    public:
        database(string host, unsigned short prt, string user, string pass, string dbName);
        ~database();
        bool connect();
        void close();
        vector<pair<int,string>> getStreams();
        bool createRecord(int streamID, string path);
        bool switchOnlineStatus(int streamID);
        bool setAllOffline();
};


#endif //RECORDING_DATABASE_H
