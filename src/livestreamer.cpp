#include "livestreamer.h"

bool livestreamer::start_check(string url){
    string cmd = livePath + " " + url + "  2>&1";
    return (stdOut = popen(cmd.c_str(),"r")) != nullptr;
}

bool livestreamer::recording(string url, string filename, string path){
    string cmd = livePath + " " + url + " best -o " + path + "/"+ filename + ".flv  2>&1";
     if((stdOut = popen(cmd.c_str(),"r")) != nullptr){
         stdNum = fileno(stdOut);
         fcntl(stdNum, F_SETFL, O_NONBLOCK);
         return true;
     }
    else return false;
}


bool livestreamer::end_check(){
    char buff[512];
    string output;
    while(fgets(buff, sizeof(buff), stdOut)!=NULL)
        output += buff;
    pclose(stdOut);
    return output.find("Available streams") != string::npos;

}

bool livestreamer::is_recording(){
    char buff[512];
    ssize_t r = read(stdNum, buff, 512);
    if (r == -1 && errno == EAGAIN)
        return true;
    else if (r > 0)
        return true;

    else{
        pclose(stdOut);
        return false;
    }
}

