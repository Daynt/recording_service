#ifndef RECORDING_LIVESTREAMER_H
#define RECORDING_LIVESTREAMER_H

#include <string>
#include <fcntl.h>
#include <unistd.h>

using namespace std;

class livestreamer {
    string livePath = "livestreamer";
    FILE *stdOut;
    int stdNum;
    public:
        bool start_check(string url);
        bool recording(string url, string filename, string path);
        bool is_recording();
        bool end_check();
};


#endif //RECORDING_LIVESTREAMER_H
