#include <iostream>
#include "stream.h"
#include "config.h"

using namespace std;


int main ( int argc, char *argv[] ){
    config conf("config.json");

    if(!conf.load()) {
        cerr << "[-] Unable to open config file" << endl;
    }
    if(!conf.check())
        return 0;
    database db(conf.file["db_host"].GetString(), conf.file["db_port"].GetInt(),conf.file["db_username"].GetString(), conf.file["db_password"].GetString(),conf.file["db_name"].GetString());
    if(!db.connect()){
        cerr << "[-] Unable to connect recording db" << endl;
        return 0;
    }

    //TODO check if livestreamer and ffmpeg installed
    vector <stream*> checking;
    vector <pair<int,string>> urls;
    vector <stream*> streams;
    string recordFolder = conf.file["recordsFolder"].GetString();
    if (-1 == access(recordFolder.c_str(), W_OK))
    {
        cout << "[-] Unable to write in records folder" << endl;
        return 0;
    }

    //reset online status
    db.setAllOffline();
    int running = 1;
    while(running){
        // check if file changed every second
        urls = db.getStreams();
        //check if streaming
        if (!urls.empty()){
            for (int l = 0; l < urls.size(); ++l) {
                bool found = false;
                // check if recording
                for (int j = 0; j < streams.size(); ++j) {
                    if (urls[l].second == streams[j]->url) {
                        found = true;
                        break;
                    }
                }
                if (!found){
                    stream* streamURL = new stream(urls[l].first, urls[l].second, recordFolder);
                    checking.push_back(streamURL);
                    streamURL->start_check();
                }
                if (checking.size() >= 10 or urls.size() - 1 <= l) {
                    for (int i = 0; i < checking.size(); ++i) {
                        if (checking[i]->end_check()){
                            checking[i]->recording();
                            db.switchOnlineStatus(checking[i]->streamID);
                            streams.push_back(checking[i]);
                        }
                        else
                            delete(checking[i]);
                    }
                    checking.clear();
                }
            }
        }

        // check recording status
        if(!streams.empty()) {
            for (auto it = streams.begin(); it != streams.end();) {
                if (!(*it)->is_recording()){
                    db.createRecord((*it)->streamID, recordFolder + "/" + (*it)->filename + ".flv");
                    db.switchOnlineStatus((*it)->streamID);
                    delete (*it);
                    it = streams.erase(it);
                }
                else
                    ++it;
            }
        }

        //display status
        //cout << "\rURLs, " << urls.size() << " Recording " << streams.size() << "        " << flush;
        usleep(1000000);

    }

    return 0;

}