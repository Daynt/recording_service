//
// Created by user on 28/05/15.
//

#include "mysqlWrapper.h"

mysqlWrapper::mysqlWrapper(string host, unsigned short prt, string user, string pass, string dbName){
    hostname = host;
    port = prt;
    username = user;
    password = pass;
    databaseName = dbName;
}


bool mysqlWrapper::connect() {
    db = mysql_init(NULL);

    if (db == NULL)
    {
        fprintf(stderr, "%s\n", mysql_error(db));
        return false;
    }


    if (!mysql_real_connect(db, hostname.c_str(), username.c_str(), password.c_str(), databaseName.c_str(), port, NULL, 0)) {
        fprintf(stderr, "%s\n", mysql_error(db));
        mysql_close(db);
        return false;
    }

    return true;
}

void mysqlWrapper::close(){
    mysql_close(db);

};




bool mysqlWrapper::query(string query){
    if (mysql_real_query(db,query.c_str(), query.length())){
        fprintf(stderr, "%s", mysql_error(db));
        return false;
    }
    return true;
}

string mysqlWrapper::escape(string str){
    ulong bytes = str.length();
    char resultStr[(2 * bytes)+1];
    mysql_real_escape_string(db, resultStr,str.c_str(), bytes);
    return resultStr;
}


vector<vector<string>> mysqlWrapper::fetchAll() {
    MYSQL_RES *result = mysql_store_result(db);
    int num_fields = mysql_num_fields(result);
    MYSQL_FIELD *field;
    MYSQL_ROW row;
    vector<vector <string>> rows;
    while ((row = mysql_fetch_row(result)))
    {
        vector <string> rowVector;
        for(int i = 0; i < num_fields; i++)
        {
            rowVector.push_back(row[i]);
        }
        rows.push_back(rowVector);
    }
    mysql_free_result(result);
    return rows;
}






