//
// Created by user on 28/05/15.
//

#ifndef RECORDING_MYSQLWRAPPER_H
#define RECORDING_MYSQLWRAPPER_H


#include <string.h>
#include <string>
#include <vector>
#include <mysql/mysql.h>


using namespace std;

class mysqlWrapper {
    MYSQL *db;
    string hostname;
    unsigned short port;
    string databaseName;
    string username;
    string password;
    public:
        mysqlWrapper(string host, unsigned short port, string user, string pass, string dbName);
        bool connect();
        bool query(string query);
        string escape(string str);
        vector <vector<string>> fetchAll();
        void close();
};


#endif //RECORDING_MYSQLWRAPPER_H
