#include "stream.h"


stream::stream(int id, string uri, string recordFolder) {
    streamID = id;
    url = uri;
    path = recordFolder;
}


bool stream::recording() {
    regex rgx("http[s]?:\\/\\/.*?\\/([^\\/]+)");
    smatch result;
    regex_search(url, result, rgx);
    if(result.size() == 2)
        filename = result[1];
    else
        filename = "noname";
    char time_buf[21];
    time_t now;
    time(&now);
    strftime(time_buf, 21, "_%H:%M:%S_%m-%d-%Y", localtime(&now));
    filename.append(time_buf);
    return livestr.recording(url, filename, path);
}

bool stream::start_check(){
    return livestr.start_check(url);

}

bool stream::end_check() {
    return livestr.end_check();
}

bool stream::is_recording() {
    return livestr.is_recording();
}
