//
// Created by user on 5/17/15.
//

#ifndef RECORDING_STREAM_H
#define RECORDING_STREAM_H

#include <time.h>
#include <string>
#include <regex>
#include "database.h"
#include "livestreamer.h"

using namespace std;

class stream {

    livestreamer livestr;

public:
    //bool isStreaming();
    int streamID;
    string url;
    string filename;
    string path;
    bool is_recording();
    bool recording();
    bool start_check();
    bool end_check();
    stream(int id, string url, string recordFolder);
};


#endif //RECORDING_STREAM_H
